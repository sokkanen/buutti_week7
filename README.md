# DAY 1

## Task 1 - Todo application

Create a React application with an input bar and a button to add a new Todo item, and a view to see all todos in the list
- Each item should be toggleable for done or not done
- Each added item to the list should also be able to be removed / discarded

- **EXTRA** Give the UI a nice look

## **EXTRA** Task 2 - Memory game (difficult)

Create a memory game, i.e. you have 16 cards, or 8 pairs of cards, with some symbols or images (you choose). When the game starts, every card should be the same, and then the player can click one card to reveal it, then click another card to reveal it too. If the cards are the same, they should be removed. If the cards are not the same, they should be hidden again.
- **EXTRA:** Allow the player to choose the amount of cards to be played with between 2 pairs (4 cards) and 32 pairs (64 cards).
- **EXTRA:** Count the number of tries to complete the game.
- **EXTRA: (harder)** Add some animation when the cards open/close, meaning they don't instantly just flash open or close, but are revealed over 1 second in some animation.

# DAY 2

## Fake Store API 

Create a user interface for a fake store using the Fake Store Api's **products** route.

https://fakestoreapi.com/docs

Your UI should support all CRUD operations (these include create, read, update and delete).
That means create a new product, view all products, view one product, update/modify a product and delete a product.

Create views 
 * to list all the products
 * to view one product in more detail (accessed by clicking one product in the list of all products). In this view you should also be able to update or delete that product.
 * with a form to add a new product

In the list of all products, implement a filtering mechanism to view only one category using the categories provided in by the "all products" data from the API.
This means the dropdown doesn't have any choices before you have received the data from the api.

When adding a new product, you will also need to provide a category for that product. The user should select a category from a dropdown containing the same choices that you received in the list of all products.

When any modification is done to any product, or any new product is added, make sure to to also update the local state after that request has been made to the backend, and receiving the "ok" message back.

Make sure that the products updates in view that show all the products everytime some of the CRUD operations have been executed.

UI doesn't have to look anything like screenshots below. 

Default filter for the products list:  
![](products-filter.png)  

Default dropdown for adding new product (exclude "add new category" if not implementing extra assignment):  
![](newproduct-category-dropdown.png)  

**EXTRA:** Allow the user to add a new category when adding a new product by selecting the option "Add new category" (means add one extra option in the dropdown). 
* If the user selects this option, replace the dropdown with an input bar for the user to type in, and when he presses complete, return the dropdown on the screen with the newly added category selected.

Add new product text input after "Add new category" selected:  
![](newproduct-category-text.png)  

Filter for products list when a new category added along with a new product:  
![](products-filter-new-category.png)  

Dropdown for adding new product when new category added:  
![](newproduct-category-new-category.png) 

**EXTRA:** add additional styles to the UI


## **EXTRA** Calculator

Create a calculator using React. Make your calculator pretty. 
![](calc.png)

## **EXTRA** Buses on a Google Map (Difficult)

This one requires you to create a Google Cloud Account. 
GCP offers 300$ for 90 days.

Create a React application, which shows buses of Tampereen seudun joukkoliikenne on a Google Map. Automatically refresh the map once per minute.

* Use this library to import Google Maps to React application: https://www.npmjs.com/package/google-map-react
* Here's how you can create your Google Maps API-key: https://developers.google.com/maps/documentation/javascript/get-api-key
* From here, you can get the realtime bus locations: http://wiki.itsfactory.fi/index.php/Journeys_API
